package com.everis.evertinder;

/**
 * Created by asegovia on 22/04/2016.
 */
public class Comment {
    public int icon;
    public String title;
    public Comment(){
        super();
    }

    public Comment(int icon, String title) {
        super();
        this.icon = icon;
        this.title = title;
    }
}
