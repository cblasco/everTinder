package com.everis.evertinder;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by asegovia on 22/04/2016.
 */
public class CommentAdapter extends ArrayAdapter<Comment> {

    Context context;
    int layoutResourceId;
    Comment data[] = null;

    public CommentAdapter(Context context, int layoutResourceId, Comment[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CommentHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new CommentHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitle);

            row.setTag(holder);
        }
        else
        {
            holder = (CommentHolder)row.getTag();
        }

        Comment comment = data[position];
        holder.txtTitle.setText(comment.title);
        holder.imgIcon.setImageResource(comment.icon);

        return row;
    }

    static class CommentHolder
    {
        ImageView imgIcon;
        TextView txtTitle;
    }
}
