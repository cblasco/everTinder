package com.everis.evertinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Intent intent = getIntent();
        final String img = intent.getStringExtra("img");
        String name = intent.getStringExtra("name");
        String desc = intent.getStringExtra("desc");
        if (name!=null) {
            TextView textView = (TextView) findViewById(R.id.textView);
            textView.setText(name);
        }
        if (desc!=null) {
            TextView textView2 = (TextView) findViewById(R.id.textView2);
            textView2.setMovementMethod(new ScrollingMovementMethod());
            textView2.setText(desc);
        }
        Picasso.with(this.getBaseContext()).load(img).into(imageView);
        ImageView gusta = (ImageView) findViewById(R.id.imageView4);
        ImageView chat = (ImageView) findViewById(R.id.imageView2);
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ChatActivity.class );

                startActivity(i);
            }
        });
        gusta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), CommentsActitity.class );

                startActivity(i);
            }
        });
    }
}
