package com.everis.evertinder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

/**
 * Created by asegovia on 22/04/2016.
 */
public class CommentsActitity extends AppCompatActivity {
    private ListView listView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.commentlayout);

        Comment weather_data[] = new Comment[]
                {
                        new Comment(R.drawable.mujer1, "Hola Guapo. No me gusta tu pose, demasiado chulo"),
                        new Comment(R.drawable.mujer2, "Hola chato"),
                        new Comment(R.drawable.mujer3, "Guapo"),
                        new Comment(R.drawable.mujer4, "Creo que eres guapo")
                };

        CommentAdapter adapter = new CommentAdapter(this,
                R.layout.listview_item_row, weather_data);


        listView1 = (ListView)findViewById(R.id.listView1);

        View header = (View)getLayoutInflater().inflate(R.layout.listview_header_row, null);
        listView1.addHeaderView(header);

        listView1.setAdapter(adapter);
    }
}
