package com.everis.evertinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ChatActivity extends AppCompatActivity {
    private ArrayList list;
    private  ArrayAdapter<String> myAdapter;
    int respuestas = 0;
    private void replyBot(){

        switch (respuestas){
            case 0:
                list.add("soy un robot del futuro, pregunta lo que quieras");
                break;
            case 1:
                list.add("Encantado de conocerte");
                break;
            case 2:
                list.add("no sabria que decir");
                break;
            case 3:
                list.add("me estas sonrojando");
                break;
            case 4:
                list.add("uff, tu también :)");
                break;
            default:
                list.add("lo siento no puedo contestarte a eso");
                break;
        }
        respuestas++;

        myAdapter.notifyDataSetChanged();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Button button = (Button) findViewById(R.id.btSend);
        final TextView mensaje = (TextView) findViewById(R.id.etMessage);
        ListView mesajeList = (ListView) findViewById(R.id.lvChat);
        list = new ArrayList<String>();

        myAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
        mesajeList.setAdapter(myAdapter);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                list.add(mensaje.getText().toString());
                myAdapter.notifyDataSetChanged();
                mensaje.setText("");
                replyBot();

            }
        });

    }
}
